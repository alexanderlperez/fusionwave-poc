angular.module('fusion-app')
    .directive("fusionMuteButton",
    [function () {
        return {
            restrict: "E",
            require: "^videogular",
            templateUrl: function (elem, attrs) {
                return attrs.vgTemplate;
            },
            link: function (scope, elem, attr, API) {
                var isMuted = false;
                var UP = 38;
                var DOWN = 40;
                var CHANGE_PER_PRESS = 0.05;

                scope.onClickMute = function onClickMute() {
                    if (isMuted) {
                        scope.currentVolume = scope.defaultVolume;
                    }
                    else {
                        scope.currentVolume = 0;
                        scope.muteIcon = {mute: true};
                    }

                    isMuted = !isMuted;

                    API.setVolume(scope.currentVolume);
                };

                scope.onSetVolume = function onSetVolume(newVolume) {
                    scope.currentVolume = newVolume;

                    isMuted = (scope.currentVolume === 0);

                    // if it's not muted we save the default volume
                    if (!isMuted) {
                        scope.defaultVolume = newVolume;
                    }
                    else {
                        // if was muted but the user changed the volume
                        if (newVolume > 0) {
                            scope.defaultVolume = newVolume;
                        }
                    }

                    var percentValue = Math.round(newVolume * 100);
                    if (percentValue == 0) {
                        scope.muteIcon = {mute: true};
                    }
                    else if (percentValue > 0 && percentValue < 25) {
                        scope.muteIcon = {level0: true};
                    }
                    else if (percentValue >= 25 && percentValue < 50) {
                        scope.muteIcon = {level1: true};
                    }
                    else if (percentValue >= 50 && percentValue < 75) {
                        scope.muteIcon = {level2: true};
                    }
                    else if (percentValue >= 75) {
                        scope.muteIcon = {level3: true};
                    }
                };

                scope.defaultVolume = 1;
                scope.currentVolume = scope.defaultVolume;
                scope.muteIcon = {level3: true};

                //Update the mute button on initialization, then watch for changes
                scope.onSetVolume(API.volume);
                scope.$watch(
                    function () {
                        return API.volume;
                    },
                    function (newVal, oldVal) {
                        if (newVal != oldVal) {
                            scope.onSetVolume(newVal);
                        }
                    }
                );
            }
        }
    }]
);
