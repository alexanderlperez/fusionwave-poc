function SingleThemeController($stateParams) {
    this.title = $stateParams.title;
    this.playlist = $stateParams.playlist;

    console.log(this.playlist);
}

angular
    .module('fusion-app')
    .controller('SingleThemeController', ['$stateParams', SingleThemeController])
