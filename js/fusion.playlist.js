function PlaylistController($scope, PlayerService, FilterService, TEMP_SONGS) {
    var self = this;

    this.playlist = TEMP_SONGS;
    this.playSong = PlayerService.playSong;

    this.limitByFilters = function (filters) {
        // take each of the filters and progressively whittle away non-matching items

        if (0 === filters.length) {
            return TEMP_SONGS;
        }

        var finalSongs = TEMP_SONGS.slice(); //copy

        filters.forEach(function (filter) {
            finalSongs = finalSongs.filter(function (song) {
                return song[filter.type] == filter.name;
            })
        })

        return finalSongs;
    }

    $scope.$watch(
        function () {
            return FilterService.filters;
        },
        function (filters) {
            self.playlist = self.limitByFilters(filters);
        },
        true // do a deep comparison, won't work otherwise 
    );
}

angular
    .module('fusion-app')
    .controller('PlaylistController', ['$scope', 'PlayerService', 'FilterService', 'TEMP_SONGS', PlaylistController])
