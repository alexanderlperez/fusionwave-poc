function ThemesController(TEMP_SONGS) {
    var self = this;

    this.lists = [
        {
            name: 'Games',
            img: '../img/house.jpg',
            playlist: TEMP_SONGS.slice(0, 5),
        },
        {
            name: 'Parties',
            img: '../img/party.jpg',
            playlist: TEMP_SONGS.slice(2, 8),
        },
        {
            name: 'Funerals',
            img: '../img/funeral.jpg',
            playlist: TEMP_SONGS.slice(3, -2),
        },
        {
            name: 'Weddings',
            img: '../img/wedding.jpg',
            playlist: TEMP_SONGS.slice(4, -1),
        },
        {
            name: 'Graduations',
            img: '../img/graduation.jpg',
            playlist: TEMP_SONGS.slice(6, -1),
        },
        {
            name: 'Birthdays',
            img: '../img/birthday.jpg',
            playlist: TEMP_SONGS.slice(7, -2),
        },
        {
            name: 'Sporting Events',
            img: '../img/sports.jpg',
            playlist: TEMP_SONGS.slice(0, 5),
        },
        {
            name: 'Bar Mitzvas',
            img: '../img/barmitzvah.jpg',
            playlist: TEMP_SONGS.slice(1,5),
        },
    ];
}

angular
    .module('fusion-app')
    .controller('ThemesController', ['TEMP_SONGS', ThemesController])
