function FilterTagsController(FilterService) {
    this.filters = FilterService.filters;
    this.removeFilter = FilterService.toggleFilter;
}

angular
    .module('fusion-app')
    .controller('FilterTagsController', ['FilterService', FilterTagsController])
