angular
    .module('fusion-app', [
        'ui.router',
        'ngSanitize',
        'ngAnimate',
        'com.2fdevs.videogular', 
        'com.2fdevs.videogular.plugins.controls', 
        'com.2fdevs.videogular.plugins.overlayplay',
        'com.2fdevs.videogular.plugins.poster',
        'com.2fdevs.videogular.plugins.buffering',
    ])
    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('playlist', {
                url:'/browse',
                templateUrl: '../templates/playlist.html'
            })
            .state('themes', {
                url:'/themes',
                templateUrl: '../templates/themes.html'
            })
            .state('theme', {
                url:'/theme',
                templateUrl: '../templates/theme-single.html',
                params: { 
                    title: null,
                    playlist: null,
                },
            })

        $urlRouterProvider.otherwise('browse');
    })
    .constant('TEMP_SONGS', [ 
        {
            src: "../songs/01_Reims.mp3",
            title: "Reims",
            length: "3:55",
            genre: "Electronic",
            mood: 'Uplifting',
            instrument: 'Drums',
            type: "audio/mpeg"
        },
        {
            src: "../songs/Jay_Z--On_To_The_Next_One_(Live_In_Brooklyn)_ITunes.mp3",
            title: 'On To The Next One',
            length: '3:37',
            genre: 'Hip Hop',
            mood: 'Serious',
            type: "audio/mpeg"
        },
        {
            src: "../songs/Justice_-_D.A.N.C.E._(Official_Video).mp3",
            title: 'D.A.N.C.E',
            length: '3:01',
            genre: 'Electronic',
            mood: 'Happy',
            type: "audio/mpeg"
        },
        {
            src: "../songs/NEED_(remix).mp3",
            title: 'NEED',
            length: '4:33',
            genre: 'Pop',
            mood: 'Ecstatic',
            type: "audio/mpeg"
        },
        {
            src: "../songs/The_Gaslamp_Killer_-_Keep_It_Simple_Stupid_(with_Shigeto).mp3",
            title: 'Keep It Simple',
            length: '1:47',
            genre: 'World',
            mood: 'Tense',
            type: "audio/mpeg"
        },
        {
            src: "../songs/The_Promise_-_J.P._Perez.mp3",
            title: 'The Promise',
            length: '3:43',
            genre: 'Soul',
            mood: 'Peaceful',
            type: "audio/mpeg"
        },
        {
            src: "../songs/Tours_-_01_-_Enthusiast.mp3",
            title: 'The Enthusiast',
            length: '2:51',
            genre: 'Electronic',
            mood: 'Carefree',
            type: "audio/mpeg"
        },
        {
            src: '../songs/02_-_Favorite_Secrets.mp3',
            title: 'Favorite Secrets',
            length: '1:15',
            genre: 'Pop',
            mood: 'Chill',
            type: "audio/mpeg"
        },
        {
            src: '../songs/Black_Ant_-_01_-_Fater_Lee.mp3',
            title: 'Fater Lee',
            length: '2:22',
            genre: 'Hip Hop',
            mood: 'Eerie',
            instrument: 'Drums',
            type: "audio/mpeg",
        },
        {
            src: '../songs/BoxCat_Games_-_10_-_Epic_Song.mp3',
            title: 'Epic Song',
            length: '0:54',
            genre: 'Soundtrack',
            mood: 'Uplifting',
            instrument: 'Drums',
            type: "audio/mpeg"
        },
        {
            src: '../songs/Broke_For_Free_-_01_-_Night_Owl.mp3',
            title: 'Night Owl',
            length: '3:14',
            genre: 'Electronic',
            mood: 'Chill',
            type: "audio/mpeg"
        },
        {
            src: '../songs/Gillicuddy_-_05_-_Springish.mp3',
            title: 'Springish',
            length: '2:23',
            genre: 'Rock',
            mood: 'Contemplative',
            type: "audio/mpeg"
        },
        {
            src: '../songs/Jahzzar_-_05_-_Siesta.mp3',
            title: 'Siesta',
            length: '2:19',
            genre: 'Rock',
            mood: 'Love',
            type: "audio/mpeg"
        },
        {
            src: '../songs/Jason_Shaw_-_RUNNING_WATERS.mp3',
            title: 'Running Waters',
            length: '2:46',
            genre: 'Folk',
            mood: 'Somber',
            type: "audio/mpeg"
        },
        {
            src: '../songs/Kai_Engel_-_04_-_Moonlight_Reprise.mp3',
            title: 'Moonlight',
            length: '3:01',
            genre: 'Classical',
            mood: 'Sad',
            type: "audio/mpeg"
        },
        {
            src: '../songs/Monk_Turner__Fascinoma_-_01_-_Its_Your_Birthday.mp3',
            title: "It's Your Birthday",
            length: '0:35',
            genre: 'Pop',
            mood: 'Happy',
            type: "audio/mpeg"
        },
        {
            src: '../songs/Podington_Bear_-_Starling.mp3',
            title: 'Starling',
            length: '1:45',
            genre: 'Electronic',
            mood: 'Contemplative',
            type: "audio/mpeg"
        },
        {
            src: '../songs/The_Kyoto_Connection_-_09_-_Hachiko_The_Faithtful_Dog.mp3',
            title: 'Hachiko The Faithful',
            length: '3:05',
            genre: 'Pop',
            mood: 'Uplifting',
            type: "audio/mpeg"
        },
        {
            src: '../songs/Tours_-_01_-_Enthusiast.mp3',
            title: 'Enthusiast',
            length: '2:51',
            genre: 'Electronic',
            mood: 'Ecstatic',
            type: "audio/mpeg"
        },
    ])
    .constant('GENRES', [
        'Alternative',
        'Ambient',
        'Blues',
        'Cinematic',
        'Classical',
        'Country',
        'Electronic',
        'Folk',
        'Hip Hop',
        'Indie',
        'Jazz',
        'Pop',
        'Post Rock',
        'R&B',
        'Rock',
        'Singer-Songwriter',
        'Soul',
        'Spoken Word',
        'Vintage',
        'World',
    ])
    .constant('MOODS', [
        'Angry',
        'Carefree',
        'Chill',
        'Contemplative',
        'Ecstatic',
        'Eerie',
        'Happy',
        'Love',
        'Peaceful',
        'Sad',
        'Serious',
        'Somber',
        'Tense',
        'Uplifting',
    ])
    .constant('INSTRUMENTS', [
        'Accordion',
        'Acoustic Guitar',
        'Ambient Sounds',
        'Banjo',
        'Bass',
        'Bells',
        'Cello',
        'Claps',
        'Clav',
        'Drums',
        'Electric Guitar',
        'Fiddle',
        'Harmonica',
        'Harp',
        'Harpsichord',
        'Horns',
        'Humming',
        'Kazoo',
        'Mandolin',
        'Nature Sounds',
        'Organ',
        'Percussion',
        'Piano',
        'Rhodes',
        'Saxophone',
        'Snaps',
        'Sound FX',
        'Steel Guitar',
        'Stomps',
        'String Bass',
        'Strings',
        'Synth',
        'Tape Noise',
        'Theremin',
        'Turntable',
        'Ukulele',
        'Viola',
        'Violin',
        'Vocoder',
        'Whistling',
        'Woodwinds',
        'World',
        'Wurlitzer',
    ])

