function FilterService() {
    var filters = [];

    function toggleFilter(filterObj) {
        // check that the filter is in the array. 
        // - yes, remove it
        // - no, add it

        function matchingFilters(f) {
            return f.name == filterObj.name && f.type == filterObj.type; 
        }

        function getLastMatchIndex(filterObj) {
            var index;

            filters.forEach(function (f, i) {
                if (matchingFilters(f)) {
                    index = i;
                }
            })

            return index;
        }

        var hasFilter = filters.filter(matchingFilters).length > 0;

        if (hasFilter) {
            filters.splice(getLastMatchIndex(filterObj), 1);
        } else {
            filters.push(filterObj);
        }
    }

    function clearAllFilters() {
        filters.splice(0);
    }

    function hasFilter(filterObj) {
        return filters.filter(function (f) {
            return f.name == filterObj.name && f.type == filterObj.type; 
        }).length > 0;
    }

    return {
        filters: filters,
        clearAllFilters: clearAllFilters,
        toggleFilter: toggleFilter,
        hasFilter: hasFilter,
    }
}

angular
    .module('fusion-app')
    .factory('FilterService', FilterService)
