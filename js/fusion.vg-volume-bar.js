angular.module("fusion-app")
    .directive("fusionVolumeBar",
    ["VG_UTILS", function (VG_UTILS) {
        return {
            restrict: "E",
            require: "^videogular",
            templateUrl: function (elem, attrs) {
                return attrs.vgTemplate;
            },
            link: function (scope, elem, attr, API) {
                var isChangingVolume = false;
                var volumeBackElem = angular.element(elem[0].getElementsByClassName("volumeBackground"));
                var volumeValueElem = angular.element(elem[0].getElementsByClassName("volumeValue"));

                function volumeByOffset(event) {
                    event = VG_UTILS.fixEventOffset(event);
                    var volumeWidth = parseInt(volumeBackElem.prop("offsetWidth"));
                    var value = event.offsetX * 100 / volumeWidth;
                    var volValue = value / 100;

                    API.setVolume(volValue);
                }

                scope.onClickVolume = function onClickVolume(event) {
                    volumeByOffset(event);
                };

                scope.onMouseMoveVolume = function onMouseMoveVolume(event) {
                    if (isChangingVolume) {
                        volumeByOffset(event);
                    }
                };

                scope.updateVolumeView = function updateVolumeView(value) {
                    value = value * 100;
                    volumeValueElem.css("width", value + "%");
                };

                scope.onMouseDownVolume = function onMouseDownVolume() {
                    isChangingVolume = true;
                };

                scope.onMouseUpVolume = function onMouseUpVolume() {
                    isChangingVolume = false;
                };

                scope.onMouseLeaveVolume = function onMouseLeaveVolume() {
                    isChangingVolume = false;
                };

                //Update the volume bar on initialization, then watch for changes
                scope.updateVolumeView(API.volume);
                scope.$watch(
                    function () {
                        return API.volume;
                    },
                    function (newVal, oldVal) {
                        if (newVal != oldVal) {
                            scope.updateVolumeView(newVal);
                        }
                    }
                );
            }
        }
    }]
);
