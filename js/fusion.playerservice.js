function PlayerService($timeout) {
    var API = null;
    var remoteConfig = null;

    function playSong(songObj) {
        API.stop();
        remoteConfig.currentSong = [songObj];
        $timeout(API.play.bind(API), 100);
    }

    function setApi(api) {
        API = api;
    }

    function setConfig(config) {
        remoteConfig = config;
    }

    return {
        setApi: setApi,
        setConfig: setConfig,
        playSong: playSong,
    }
}

angular
    .module('fusion-app')
    .factory('PlayerService', ['$timeout', PlayerService])
