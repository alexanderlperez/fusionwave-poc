function FilterController(FilterService, GENRES, MOODS, INSTRUMENTS) {
    var self = this;

    this.GENRES = GENRES;
    this.MOODS = MOODS;
    this.INSTRUMENTS = INSTRUMENTS;

    this.show = {
        genre: false,
        mood: false,
        instruments: false,
    }

    this.toggleFilter = FilterService.toggleFilter;
    this.hasFilter = FilterService.hasFilter;
    this.clearAllFilters = FilterService.clearAllFilters;

    this.toggleOpen = function (category) {
        self.show[category] = !self.show[category];
    }
}

angular
    .module('fusion-app')
    .controller('FilterController', ['FilterService', 'GENRES', 'MOODS', 'INSTRUMENTS', FilterController])
