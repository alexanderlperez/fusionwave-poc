function PlayerController($scope, $sce, PlayerService) {
    var self = this;
    var API = null;

    this.config = {
        themeUrl: "../css/videogular.css",
        currentSong: [{
            src: "../songs/01_Reims.mp3",
            title: "Reims",
            length: "3:55",
            genre: "Electronic",
            type: "audio/mpeg"
        }],
    }

    this.onPlayerReady = function (api) {
        API = api;
        PlayerService.setApi(api);
        PlayerService.setConfig(self.config);
    };
}

angular
    .module('fusion-app')
    .controller('PlayerController', ['$scope', '$sce', 'PlayerService', PlayerController])
